class AddAasmStateToInvitations < ActiveRecord::Migration[5.2]
  def change
    add_column :invitations, :aasm_state, :string
  end
end
