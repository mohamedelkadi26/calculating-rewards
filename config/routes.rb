Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root controller: 'application', action: 'index'
  post '/calculate_reward', to: 'application#calculate_reward'
end
