require 'rails_helper'

RSpec.describe Invitation, type: :model do
  describe '#accept!' do
    let(:invitee) { Customer.create name: 'invitee' }
    let(:inviter) { Customer.create name: 'inviter' }
    let(:invitation) { Invitation.create(invitee: invitee, inviter: inviter) }

    it 'can accepted by invitee who did not accept invitations before' do
      expect(invitation).to transition_from(:pending).to(:accepted).on_event(:accept)
    end

    it 'can not be accepted by invitee who accepted invitation before' do
      prev_invitation = Invitation.create(invitee: invitee, inviter: inviter)
      prev_invitation.accept!

      expect { invitation.accept! }.to raise_error(AASM::InvalidTransition)
    end
  end
end
