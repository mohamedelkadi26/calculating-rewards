require 'rails_helper'

def create_invitations_and_invitees(customer, number)
  number.times { customer.invite_friend }
  friends = []
  number.times { |i| friends << Customer.create(name: "fr_#{i}") }

  customer.sent_invitations.each_with_index { |inv, indx| inv.update(invitee: friends[indx]) }
end

describe RewardingSystem::Calculator do
  describe '#calculate_reward' do
    subject { described_class.new(customer).calculate_reward }
    let!(:customer) { Customer.create!(name: 'A') }

    context 'rewarding for direct invitations' do
      it 'rewarding 3 for 3  invitations' do
        create_invitations_and_invitees(customer, 3).each(&:accept!)

        expect(subject).to eq(3)
      end

      it 'does not count the pending invitations' do
        create_invitations_and_invitees(customer, 3)

        expect(subject).to eq(0)
      end
    end

    context 'rewarding for invitations of invitees' do
      it 'rewarding be 2.5 for 2 invitees one of them invited a friend' do
        accepted_invitations = create_invitations_and_invitees(customer, 2).each(&:accept!)
        invited_friend = accepted_invitations.first.invitee
        create_invitations_and_invitees(invited_friend, 1).each(&:accept!)

        expect(subject).to eq(2.5)
      end

      it 'rewarding 2.75 if the friend of the friend sent invitation' do
        accepted_invitations = create_invitations_and_invitees(customer, 2).each(&:accept!)
        invited_friend = accepted_invitations.first.invitee
        friend_invitation = create_invitations_and_invitees(invited_friend, 1).each(&:accept!)
        friend_of_friend = friend_invitation.first.invitee
        create_invitations_and_invitees(friend_of_friend, 1).each(&:accept!)

        expect(subject).to eq(2.75)
      end
    end
  end
end