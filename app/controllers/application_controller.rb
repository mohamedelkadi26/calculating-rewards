class ApplicationController < ActionController::Base
  def index
  end

  def calculate_reward
    uploaded_io = params[:customers]
    csv = ::CSV.parse(uploaded_io.read)
    @results = CsvHandlers::CustomersRewardReport.new(csv).process
  end
end
