class Invitation < ApplicationRecord
  include AASM

  # associations
  belongs_to :inviter, class_name: Customer.name
  belongs_to :invitee, class_name: Customer.name, optional: true

  aasm do
    state :pending, initial: true
    state :accepted

    event :accept do
      transitions from: :pending, to: :accepted, guard: :can_be_accepted?
    end
  end

  private

  def can_be_accepted?
    invitee_exists? && first_accepted_invitation?
  end

  def first_accepted_invitation?
    invitee.received_invitations.accepted.count.zero?
  end

  def invitee_exists?
    invitee.present?
  end
end
