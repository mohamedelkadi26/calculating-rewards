class Customer < ApplicationRecord
  has_many :sent_invitations, class_name: Invitation.name,
                              foreign_key: :inviter_id
  has_many :received_invitations, class_name: Invitation.name,
                                  foreign_key: :invitee_id

  def invite_friend
    sent_invitations.create!
  end

  def accept_first_invitation
    received_invitations.first.accept!
  end
end
