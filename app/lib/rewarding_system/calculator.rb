module RewardingSystem
  class Calculator
    attr_reader :customer, :level

    def initialize(customer, level = 0)
      @customer = customer
      @level = level
    end

    def calculate_reward
      total_reward = rewardable_invitations.count * reward
      return total_reward unless rewardable_invitations.present?

      rewardable_invitations.each do |inv|
        total_reward += self.class.new(inv.invitee, level + 1)
                          .calculate_reward
      end

      total_reward
    end

    def reward
      0.5**level
    end

    private

    def rewardable_invitations
      customer.sent_invitations.accepted
    end

  end
end