module CsvHandlers
  class CustomersRewardReport
    Event = Struct.new(:actor, :action, :object)

    attr_reader :csv_data

    def initialize(csv_data)
      @csv_data = csv_data
    end

    def process
      clean_tables
      events.each { |event| process_event(event) }
      customers_rewards_table
    end

    def clean_tables
      Customer.delete_all
      Invitation.delete_all
    end

    def customers_rewards_table
      Customer.all.map do |customer|
        {
          name: customer.name,
          reward: calculate_reward(customer)
        }
      end
    end

    private

    def process_event(event)
      case event.action.to_sym
      when :recommends
        handle_recommends(event)
      when :accepts
        handle_accepts(event)
      end
    end

    def handle_recommends(event)
      inviter = Customer.find_or_create_by(name: event.actor)
      invitee = Customer.find_or_create_by(name: event.object)
      Invitation.create(inviter: inviter, invitee: invitee)
    end

    def handle_accepts(event)
      customer = Customer.find_by(name: event.actor)
      customer.accept_first_invitation
    end

    def data_to_events
      csv_data.map do |row|
        Event.new(row[0], row[1], row[2])
      end
    end

    def calculate_reward(customer)
      RewardingSystem::Calculator.new(customer).calculate_reward
    end

    def events
      @events ||= data_to_events
    end
  end
end