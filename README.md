**About the implementation:**

I had two options in my mind after reading the task which at the first glance tree data struct 

- first one: implementing the task using plain ruby and use Sintra to serve the simple endpoint 
- second one: use ruby on rails and make it look like the real world and use mysql database 

i decided to go with the second option and implement it using rails as i applied as ruby on rails developer and find it can be more interesting to think about it as real-world situation when having customer who can invite friends through email then they accept and register and all that is persisted in the database. then the company decided to add a rewarding system. i implemented it in a very basic way and 
much optimizations will be needed if this was used for large data sets (ex: calculating the reward in the background or using graph database such as neo4j )

**Implementation Details**

added two active record model: **Customer** , **Invitation** 
* the Customer can send an invitation to a friend which creates new invitation record (in the real world an email can be sent to a friend at this step)
* the invitee should be assigned to the invitation (as if the user registered ) then the invitation state can be  accepted, the customer 
 can only accept one invitation which makes it not possible to be rewarded on multi invitations to the same user
* most of the logic is in RewardingSystem::Calculator which use recursion n to calculate the reward for the customer
* tests written using rspecfor invitation model and calcultor [ spec/lib/rewarding_system/calculator_spec.rb , spec/models/invitation_spec.rb]


**Running the application**
- environment [ ruby-2.5.3, mysql ] should be installed 
- change the database.yml to match your database configration 
- cd to the app directory and run ./bin/rails s 


**Usage**
- go to the index you will find simple upload button to upload csv 
- upload the csv in the correct format (no validation here it's just page for testing results so make sure that it's csv file and has the correct format )
- all data will be cleared before calculations and the result with retuerned as table when you click process 
- i supposed that this page is internal page for development to just test the algorithm so i didn't write specs , do validation, or add styling . 
- the format for data is like that  
```
A	recommends	B
B	accepts	
B	recommends	C
C	accepts	
C	recommends	D
B	recommends	D
D	accepts	
```
i removed the dates as it should be sequential events 
[download sample test file](https://drive.google.com/open?id=1yA3LSN2QhV9kQzUfkIk-TDml1NpgqwhW)

if you prefer to implement it using plain ruby without mysql please ask me to reimplement 